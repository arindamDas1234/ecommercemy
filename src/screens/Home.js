import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  Dimensions,
  ScrollView,
  SafeAreaView,
  AsyncStorage,
} from 'react-native';

import {Header, Card, Button} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';

import {IconButton, Colors} from 'react-native-paper';
import {FlatList} from 'react-native-gesture-handler';

import SearchableDropdown from 'react-native-searchable-dropdown';

import Modal from 'react-native-modal';

import NetInfo from '@react-native-community/netinfo';

var storesLocation = [];

export default class Home extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isVisiable: false,
      storeName: [],
      locationName: '',
      isLoading: false,
      selected_shops: [],
      bannerImage: [
        {
          image:
            'https://www.invespcro.com/blog/images/blog-images/ecommerce-email-banner.jpg',
        },
        {
          image: 'https://i.dlpng.com/static/png/427923_preview.png',
        },
        {
          image: 'https://i.dlpng.com/static/png/431735_preview.png',
        },
      ],
      productCategory: [
        {
          cat_name: 'Fruit',
          cat_image:
            'https://post.greatist.com/wp-content/uploads/2020/04/pineapple-732x549-thumbnail-732x549.jpg',
        },
        {
          cat_name: 'Vegitable',
          cat_image:
            'https://5.imimg.com/data5/LJ/VK/MY-41533060/papaya-fruit-500x500.jpg',
        },
        {
          cat_name: 'Clothes',
          cat_image:
            'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExIVFRUXFxUVFRcVFRUVFRUVFRcWFhUXFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGC0fIB8tKy0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLSstN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAADBAIFAAEGBwj/xABDEAABAwIDBQUFBgMHAwUAAAABAAIDBBESITEFBkFRcRMiYYGRBzKhscEUQlLR4fBikvEVIyQzcoKyNENzFhdTotL/xAAZAQACAwEAAAAAAAAAAAAAAAABAwACBAX/xAAoEQACAgICAQMFAAMBAAAAAAAAAQIRAyESMUEEE1EFFCIyYUKBoRX/2gAMAwEAAhEDEQA/AOIbKiB4Kr6aW4TAXItxO3GSHmxNKsINlNIyVGHELo9j1GWausvyOg/gqJtn4ZGm2jgvoTYAvSNH8BHwXjlXBiIPivZN2c6ZvT6LdhlaZg+oO3E+Yq2l/vpBb/uP/wCRV9syhbbNNbXog2eX/wAj/wDkUqyS2ix5p8TodxQjvDTNDTZcztOndZrQCQG5loJ71zi00zXTbavguVzs8JLciQbZi9sQ8L/eC0+ifODZy/Wvo6P2PUjjXucWkYInk3FrYi1o+ZXsAruyfYnuuOXg7l5rzT2d7ViiqZY7AGWNgY+97mMElhPTP/avQTG2RhBzRypqQvD+pdtqQ4aoUzguS7WanNjd7PiPzVzQ17ZBkbpYwVqdtxAlpY93+lhI6XNgp7L2sMyynwWucUjg0YRmSbXPBOVEYLbZLg/aDtL7NTGNjzjm7mueC939Bbu/7kyG9FpNKLbOT3p2wKupkna4ua6zWXys1osBbhoT1cqcpLZ8hBtqCdFZvh0s4EHQ3tnyz4rautHNb3sXKiFZbQ2JUw27WF7LtxC9u83mCDYjMac1WAqMhNah1WFai1VJDMfZeUpyTIKUozkmQUo1okSsuokrV1CEwVu6GCt3UCExLFC62iAptmOyVrZVOxleTMyWD1EPysbCWiIsrXZzlSgqyoHLJI04ns6COS69g3VP+Hb++C8M+0WIXrmyt4IKWjY6aQC47rRm92X3W8eui6Po7aaRl+oapnnG8cH+ImH8bvndUlQ1seHHliNhcZG2titb17wMmne6NpOJziGA97P8RGTePFVU1RLK5sk78TmMDGDLCxo0DRpwzKf/AOb7jub/ANCZ/UKjUUHragE2HDiue2hHYkHqPH9VZl3qUvUN0vnfIroRwQxw4wVHPlllOXKRU0FS6ORr26tcHW8Qb/p5r2PZW0sTWyNza4A+uf1Xj1VTWzBy+S9H9kdW2YPpX5OF3s0sWkjEPIm/meSx5oeTRhn4Orqpw5q5KtkkY68ZLT4cfJekT7vA5cFCTdmJrTYZniVko1pnkO0N8apt2jDyuQT8AVxm1a6SeUukeXm5AJ/DwsAMl6bvVu5HC1zvAkc3EAmzeZyXmc57Nx0LvWx4gdNL+C0YlozZnsNRMwWcRnfjwTskF828dRqD6pNou0eqap38Dot2NJIySexuKskIDS93dBDWvJLW3/Byzt6IgqCRaSK/TC7LrkfRLO8c/mpskt0TUVsySlhd7jsDjoHYgD4d7NVbBY2Kui9rhhcAevnoqmeDA+3A5g8x+7rPngltD8D2WlGckzdJ0ZyTKyeTciRKy6hdZdEJO62Ch3W1CBLrEO62oQqdlFXrpLgLntnOV6wZBKyRtOwpmiE/RaKsfLZM0tSubKJqxPY1WTtaMTjYDlqeQ9VX1U81Rm4ljNLXu8t4XP3R4BD2rUHJrRcmx6WN7lZ9puAfVdv6fCse13/05n1Cd5aT6JxQNjGFgtz8eqnKNEE1IHX0W2zt55ro2jBs0PFQkAUiEnUteUJOiGpi2+RB5j6pWGV8Tscbi03BBaSCLcQRotuhcBllfVRjYSkSV9ounR6Duv7WZIiGVjDKw/8AdZYSN4d5uj+osfArq94vanQxxAwO+0SOF2sGJgb/AOQuHd6WJy4arxCqh7p8M/zSoKyyxKx8crou9rbeqKgunmfdzrsYBcNYzIuwDhqBfXXja1AWEp+qb3Yxb7gOltSXefvKDGJsY6FylsnTOsB0CZ7QJZug6D5LacuhbHTKFASEoEbSmmNsrbYAgsR48Ph+aHVjEwHi028nfqB6qEr7Z/vgpF4OY91ws7+E8+irPaaLQdNMLSHJMgpakGSOsHk6S6JXWiVpYVCxsKV1BbBRITWKOJYoQpKI5rpqZt2rmKdhBC6qiIDc+Sq+iIqK42KjTvN1m1jncIFDKs0o6H4dyonVVjsbhlhGpOpNtAhQ1Bz9Qo1o75/fBQiyK6mK1FI5Gd3OT/o5E4HUeqyWm5LTiP6ZopcW5eef5BPQkVDpGorK78QRi48vXJRLDy9B9SptEJiQO0v6IU1PY348kXBfLXqSf0Q9pksLSNLkG2mYBHyPqi+rZAcjMvgVTEWJHK4Vw1xdxFvRWWy9031AdUdrAyKMs7QSue0m7g2ws0jvEgC5Fy4JGT5GQKjaDe/a1rNY3S2YaAdfEFAIRKl2J7iOJvy1z04KbYbD9+Kuloqy33X2GypfhknbC0BuZvcg5EjLRts+o6hfa2zhDM+IPDw02Dm6OHA+irm1MjCMPA3HAg8wRmERtQ7MlhGpJJ4nU+Ki7I+gzQAhvk5ID6wcj6ArGzNOYPzHzVuS8Apkah3DxRYH5XaUhWS3Oqns51jnoUvlsPEuKaUHhYjXl4W+KKShROHBEWfIqkbsLuBu62VFYljiS2ohZdQBJYtXWKEEbAORm7RsbKmMxvdZI+6Wk6ouXFXM1wySFC7vW55JMPR6M2cCoo+GGMqdoYrJBiPUj0yW4u63EfJMPlgGdi52fqc1V19UXnkOAXQdRRyW3J2HhqjdXEDmlgIGuv78lyuJWFHXvAwgjXiP1UhkI4/Bc3XT7k7PopGzvq3ElrXBjA4gk2FjfQcdcss1x4fJ+Ieig58n8J8v1TJW1oC0OVLo2OeQ44MTgwn3iy5wE2+8RbRBqYZJIBNgwxYhhJ45uafi05C9uOoSX2ck3fmfH6BFjpjiybwucswBkT8R6KknIKo1EwjNWdFUARyMPbEuA7sOHC7C4SNMtwSGtcxpyskmafkuj3Lc/wC0FjQTjYbi18m2c4kcRa/zztYma0CL2cu1nePVTJ5ItRSuY97C0gtc5pDveBBIsfFAMZ4GyK6AY6VrfEpZzy5HFOBqbqRkA4KEBxwLdRZjbrHVR5Jepu5jieGiEmq0FdlcMymo0rEnWW1GZ4JMS7GKWUhzRzOY8PFWN0jCwMBe4i/y8E4x+IB3MBTNHSY3BLdErqSitgLOayTQnP7Nfa4zSTQVa0tU9mROSpOVK0JyTcVaFfsEn4VpXH21Yk+7L4M/3Evg86W1pYnm8wJimCXCbo9fEZi/MW/VFK3QU0k2+iD6ZAfCQmpGE94ZHiEpIX8Vqejm+dAnBEptbhDIPJSpn2d1yVF2WLeOpRRMCkZGcQotcU/kKaLBx8VKGoLLkcQWm+eRy+dkmyVM8EW7REFi+qa7Zzc2m3dc05Agtc0tc0g6gtLh5oTWAOdmMIJzytbhmPJZIr9xK+TTpXvc57zdzjcnS5KFISMwpDIeQPwQJnP94aclPASD3oBKiZDyWYv3dLbIYpVn+Ueo9FBr80erd/dHp9UH0WXZUsKMCl2lFDzwySYsuw7GD75sL6DU+Staea4PLh00VPDCSVa0kFnC5s05dOSu1caJGSjKxhpuj077HMK/jjgY0DInqq+tqYs7ALFfIa8/LVCklQAdFGeruFXzSEnJDdIqcLENWOfaHcysSnbLSHBg4FWtrLrE46hIMTNIMnc8OXqEs1yboRk48Ra3LxV8auSKZXUGDfKeFgTqEF7uYsjVOf3fQ/MJYv8AG45FaZHPQJ70Np4qTytsGRSWxkVZavZxGhQS/mE1RC7QDyUZaYg5ei015QkC1w5lZ9ptoVv7O92QbZPU2zWszfmfgiotktAqbFJm7Jgz6nkmInXPn4qFRUXyGnILcLslcqMzxYbD+Efv4pZrs0eoqC51zyt6afBJPKJA01LfMJQ0ruSJHK7gm5agtbpcoUiCQpeaXqiC0tHDP0RZJHu1FgslYAxx8D+SXLfRZdlMiMcUNSbdZ0OG4mu4KxpIBx7x4a5KrZJb93Tkcj7AnFbwH5aeadHoU0OGodzUTITxUSb5g3HBFMVm3usjVM16oYewCPEquR4KnNVEi3BAD1UTxD3CxDusQoPEDLHmUIhWU1PlkkMGaszaiBT1G7uEWJJOnwy5JUxlM0VMXNJBIztrbPL80zF+wrO/wByROHMdRf5JeR44hNy0JHG/ndIzQuHD5JzkY0gLszkjMHBChZc26n0F1Z0FIJHWvb9El7ZoxtK7N9l3RmR0W2TyN44uqd7Gwtyy9EOOH15/1WuvKMl7C09dfVEkIdzslZaXkfhZThJGRV1J9MDNmNgUC7ktSsULIENyS5+vyCgJWjUKLhp5rUbLqqYWMR1I4N81vthfNSsAEtIrNsA4HtI010/TmgVwysOKhE7ujXiPifyUjop2qIUkjbFaHJdzufsiOYTYxfCWEdHA/wD5K5Cphwyvba2F7h0sSFi5/m4rwanjaxxn8kqaHmVc0zW2BBIyHG/DxVdTRKyLMm5ZXYD0uLrVhM0x2koI5Guta4J93S/Hoqyohc04Su13q2zHO2J0cYZKwFshaMLXDLBYeAvquXqZ7tzGaxZL9xjYy0U5jWsAU3tOqgHoIbFpmsKxbusRL0OVb7OshUVGZZAxuriAOpT9LupVzBzw0WaLm7gCegVrujsCaKtp3vAwiQXz8ClzmkrNsfTZavi9HX03sdBjaXzuD8sVgLDmBcLlt9N3GUU3ZRuu0i/HECGsJxfzi36L6Jij7q8Y9tLA2tY42A7BvUuL5AD492O3kEPSSnz/ACfaOdmnyVHms1MOZSMtO4aOV/R0j57iGOSXh/dsc8DqQLBMTbqV1v8Ao57c8B0WxtCkmclTueHWBti7h8Q4i4PguwpN32tIN7rmZGdlMztGubhe0ua4FpsHC+R8Lr6X2huZTSMDWxhttHMyNuoWL1OSUK4qzVg9v/M8Dqow2RzRwP0CBJGFa717PFPWzQglwY4C51za12fqqvHbULq4ncFZiyVyddGom2Plz5/0QKmfOwRr5m2mQ+Z+qVkGau+ihIG60WqIaiNb4eaqEFhyHVEiZZFwiw6/VZfJBIjYN+iFa6m6RYHokAjIG3P9fqpxu1TuwNmfaZuyxiO9yXFuLRugFxckgDUa3QtpbOdBPJC44jG97CRkDhcW3HWyqnug15LrdSpwGT+IM+GL81RbZgc+se1jS5z3DC0C5JcAfzTNDK5rxbiLLo9zKf8Axj5Hi7iGhp5Djb0CxZFxzN/KOhD8/TqPwyz3d9k0sjQ6eoEV/uRtxuHV7rC/krfaHsiDYyYKqRzxmGyhmBxGdjhaCF6Nsgd0J2Uqc5Lpi3CKdUfNBke10sUjcMkZ74PAg2+oVfJOL2K67e6kB2hVSAXxPA6BrWtPxBVI7ZYJuWpeTLylbD9m60UFXG7XOyHHTm111UlJcBuFaGy7i2E+iopqgr0s0czgWLpv7E/hPosR5ot9vM7TZlC5xwCW1xnnYZK+2Zuqxr2ySTYsJBABsLhV7YKYZ9sB0KMIKXjUH+crIoyXg6+T12Sarl3/AA79u0WDLEPULi/aHu4yumpZWPitE4NmY91g+HFis0gHMHELZe8hgUP/AMt/95UX1FAPvj+Yq0ZTi9HN9iD2d3suNjIwyNoa1os1rRYAcLAItRoqbdzaUcoIjdiDe70yyV+9twtUXaESVSPIPaFs1kgIeMufEeIXptPt6na1rRI0gNa0EkXIAABK5HfSjLg7Lh8kjs3btAGMxx2Ia2/dBzsLqs+WuIzhB/scPv1MJNoVL25gvy8mtH0SVDsmN7XufKWubE6RoAv3g7C1pzyF8vNQ3nqmvqZnx5MdK8t4d2+WSqnVBAJvoD6rqJfglZzn+zJSu16/IAH5FKgLWI5XOdvjxUgUbsqaBKKxyGpxDNFECtPwI+RUZE6acCHHx7QN/wDqSPkfRISnVHwQXLlFpN0TCENxCqQapZiyRrwSL5crHgmJJiSSbZ+Z9VXNflYozX3HjoeqidEY1S1AjmjeQCGuBcOYvmPRdxsHeCmqKljIo8DsyTlmMsl50w6q83GewbQga3Vzi0/yk/RZfUQTakavT5HFOK8n0bsxtmhTrZA1pcdACT0AuUSlbZoXOe0asMWz6p4Nj2L2g+LxgFvVJrQy9nk1R7QoC5zuxvck353JN1Xy79svdsQXGMiPKyk6nCr7MS/3GQ62Df8Asf8AKBU5vaM8+7E0LjvswW2U44Ke3EDzZDqf/cGX8DVi5j7IeS2p7cCn3MgmF/GU+q2L8ZT6oVU1VpT+K+BPJ/Jdtc3jKfUqQkj4yH1KolgU18Et/J6j7Ld4ooKsRY+7PhYbnLGL4D4akeYXv0LrhfGUchaQQbEEEEagjMEL6n3B28KujhmyxFoa+3B7cn/EX8wly7sZHaoZ3lprsJsvDNsbaZCXRlpJaSPyX0PtGK7T0XzP7SKfBVu5EA+YuD8grQdBmrjYCeTG0Otr3s+F0nKcgD+IX+f0TlO28bB/C35BDnpnDMgW5+RW2nRk8k2NbbS6WmepRutlewPFalgI/NHsBEIrCLhBsVNqhBh1QbYeGvDgMvmUs45orCM+mXrZLG9/JAhqVyCjdkXFE+xHmPNCmyWKOejQu5hORUoHitmmYDf65I8GGwDWuTe6dS2CvjqHhxjidikLRiIDmljbDiS5wCPsyhkqZWwU0eOR3AaNHFzjwA5r1/db2UQQxj7W4zuLhI5jbthEgaQ22jnYcTrXI1vbRKzVVF8d3Z0lFvTEWBz45Y2nRz2tIPkxxI9FzHtjrmO2U90bw4PkhaC03Hv4rZf6TkrDe7dQGIup5ZI3NGTC4uiIHAA3LT4/BV2zKIsp2xktkE0nfcBk6JuFuEgi4zL7jw63S+FaGrne/J4BHU2yKZDwUtWwgOOHS5t0vl8FlOCFUsnTpjCnACCtRjNORsScjo0QVoztjyWIvZLEnkX9tfAvLQPtoqWSM3txXX7R2s0MNhmVzdBKA4uOZ4LXzbMrxpOhFzbZLTQn6uAtOIjVKnVXFgivVvYTtwtmkpHHuvHas8Htwhw822P+1eXsivdWu520vs1dBMTYNkaHG9gGO7rr9ASfJVa0GLpn1k4Ym2Xz/wC1vZ4fMJYLvjY20hDXDCXEFhOIC7Tc2IuMivZxtsNcxrbFzgSb6NBBa3D+JxeW5crlW9PTsGI2BLjdxsMz+XghEvJeD5WoZxhb0A9P6Jk1LNCbr3zfDcelq43ERMZNbuSNaGm40D7e83rzysvn6elw3Dm4XA4XAgXBBsR6rVHL4M8sfknI+MjK10pDVjQjLhmoGEch6IRjbyCLyMrxHHStPghi10EsHALMCtbAGc3MW4An0z+iA6QAqbW8tf6/khmO6FuyAu2Ol0YMeeKmyEN6qbdf30+qlaIaER4vPlkttpQdQT1JKMArrdnYUlZOyBlxize4fcYPed1zFvEjxS7L0et+yLdxtJRCVzQJaiz3HiI8+yZ/Kbkc3Hku3e/LVUe26vs2w08XdLi2NvNrWjM+TQfRGqpMOGNt8hbyHNIbNccXRHas3dIvqvH9tb6CmZLRwtLpRjZ2uPJnaA9phFveGg4A3PBehbybVEMT5DmWtJaObtGj1svn+Glc95JN3EkuPEkm5Pql8ki84ukkLTDJNSxYS08wj1OzHBpPJE2jFeKN3IJkGmhGSLT2Is1TkZzUKSjc8XCabSOBWfK1dGrEnRtbU+yKxJHnOOnLkGm94dVqE6rUR7w6relRzJO3ZdbYeDhHIKpcMwmtovNx0QKUYjmrSZWKslBoUEDNNloGiWCBGezbp7QOGiklc5wuYtCA0tjtGSfvE6L1WlqR6rxnZc5bsgSNAxRkOaSL2N/2F3m6u246mFsjDYaEE5tcNWu8UucadmjE1JcTrhWAPwO43wngbajqvIva1uuYpTVxtvHIR2tvuSZAHo7Lz6r0zbLC+AljgHgYmO5ObmP1SGwN46Supn9q+NvdLJ43uaMJsQ4Oufd1sdCjGWwTguJ88vYhYPoibUrY2TzMiJfG2WRsb9ccbXkMdfjdoBSgrxxB9E67MrsK8fVYSo/aYyfeI11C0KiL8R9D+SfyXyUphojYi/Vadqf3qof2jHycfIDNafWN4h2dtBa1srfD4oclYaZIogGSX+3D7rPUrDO48cPT8yo5oiiyzjw2uTZd37Md8dnUQn+0PLZHloa4Mc8FjQe73QbG5J8xyXmJw8QT1KwnKwbYeASmXTpnsG629TNo7Xc9vdiiicIA7Jz7luN5HOzR42C7Ss2iC4tbkfjbS55dF820lQ+F7ZI3Fj2nE1wyIK9j3F3ogq2ODjgqBd8t7WLRq+PmBpbUJMotGvDlT7KD2k7cDHtpuNsb/PJg/wCR9FwIqsDibapbb21HVFTJMfvvJHg0d1g8mgIe0L4QUtxVkeVvfwO1G18TS1HrJP7iPxXNBX9S7/Dx9U2EUk6EzyOTTYOnryzIKR2k5xCU4puIC4SMqSdj8UpNVY12rlimsSLNFf05qBuqgz3h1RKXioWzC3nNLGX6INC3vFTqXZjopUOZKE+gw7BznNLu0Uqp3eKJTxYiAiuiS7PStn5bFd++KotxN4xS1BZI60MtmuP4X/df9D5cLq9E7BslzMQvpZeaVWqtJaoEJOLtH0jJtQU0UkspJiawuxDO9hkPAnLrdfO9bIZHOcdXEl1tLk3suk2lvg6TZkFHe7g7+9vxbER2Q88if9HiuZYVSEaGZsnJ6BxRWRuzC2ApBNoSD7IKJhRwFIBSiCb4Vt6bLFoahvL4DmpRAEUSYZEjRR3ztki4QNUaABbGOSlgPgFIzgaBCeZHaCyJBaoYSfDmUOneWuu04TmLg2NiCDmPAkeaLNCGi17uOvFCOSXIKJVMQysFufNqiHG2eqi/RJ4jsfQnAM1cvzpx4FJbI2e+Z+FtvNdVDutJgwOPoj7kY6bCsUpK0jlXahORcF0Y3OHF59E1FutGPvFJyZIyQ7HhnHZQWWLq/wD02z8SxZ7NNHl9JxWnajqtLF0TljVbwRNm8VixCfQYfsKVfvFO7M18isWIxJLsvz/0r/8AUucl95YsV5FEaHDzRAtrEERk1gWLFYhNqkFixEBtQ4u6BbWKEGo9PJQesWKyIbi1TZ0WLFCFXJqhj3lpYqMKByalacsWJMux0Oh/dX/NK9Dp1ixYs37m3036lpApLFiohrCLFixEh//Z',
        },
        {
          cat_name: 'Electrnics',
          cat_image:
            'https://www.ethiopianimporter.com/contentimageexport/Electronics.jpg',
        },
        {
          cat_name: 'Fruit',
          cat_image:
            'https://post.greatist.com/wp-content/uploads/2020/04/pineapple-732x549-thumbnail-732x549.jpg',
        },
        {
          cat_name: 'Vegitable',
          cat_image:
            'https://5.imimg.com/data5/LJ/VK/MY-41533060/papaya-fruit-500x500.jpg',
        },
        {
          cat_name: 'Clothes',
          cat_image:
            'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxITEhUTExIVFRUXFxUVFRcVFRUVFRUVFRcWFhUXFRUYHSggGBolHRUVITEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGC0fIB8tKy0tLS0tLS0tLS0tLS0tLSstLS0tLS0tLS0tLS0rLS0tLS0tLS0tLS0tLS0tLSstN//AABEIAOEA4QMBIgACEQEDEQH/xAAcAAACAgMBAQAAAAAAAAAAAAADBAIFAAEGBwj/xABDEAABAwIDBQUFBgMHAwUAAAABAAIDBBESITEFBkFRcRMiYYGRBzKhscEUQlLR4fBikvEVIyQzcoKyNENzFhdTotL/xAAZAQACAwEAAAAAAAAAAAAAAAABAwACBAX/xAAoEQACAgICAQMFAAMBAAAAAAAAAQIRAyESMUEEE1EFFCIyYUKBoRX/2gAMAwEAAhEDEQA/AOIbKiB4Kr6aW4TAXItxO3GSHmxNKsINlNIyVGHELo9j1GWausvyOg/gqJtn4ZGm2jgvoTYAvSNH8BHwXjlXBiIPivZN2c6ZvT6LdhlaZg+oO3E+Yq2l/vpBb/uP/wCRV9syhbbNNbXog2eX/wAj/wDkUqyS2ix5p8TodxQjvDTNDTZcztOndZrQCQG5loJ71zi00zXTbavguVzs8JLciQbZi9sQ8L/eC0+ifODZy/Wvo6P2PUjjXucWkYInk3FrYi1o+ZXsAruyfYnuuOXg7l5rzT2d7ViiqZY7AGWNgY+97mMElhPTP/avQTG2RhBzRypqQvD+pdtqQ4aoUzguS7WanNjd7PiPzVzQ17ZBkbpYwVqdtxAlpY93+lhI6XNgp7L2sMyynwWucUjg0YRmSbXPBOVEYLbZLg/aDtL7NTGNjzjm7mueC939Bbu/7kyG9FpNKLbOT3p2wKupkna4ua6zWXys1osBbhoT1cqcpLZ8hBtqCdFZvh0s4EHQ3tnyz4rautHNb3sXKiFZbQ2JUw27WF7LtxC9u83mCDYjMac1WAqMhNah1WFai1VJDMfZeUpyTIKUozkmQUo1okSsuokrV1CEwVu6GCt3UCExLFC62iAptmOyVrZVOxleTMyWD1EPysbCWiIsrXZzlSgqyoHLJI04ns6COS69g3VP+Hb++C8M+0WIXrmyt4IKWjY6aQC47rRm92X3W8eui6Po7aaRl+oapnnG8cH+ImH8bvndUlQ1seHHliNhcZG2titb17wMmne6NpOJziGA97P8RGTePFVU1RLK5sk78TmMDGDLCxo0DRpwzKf/AOb7jub/ANCZ/UKjUUHragE2HDiue2hHYkHqPH9VZl3qUvUN0vnfIroRwQxw4wVHPlllOXKRU0FS6ORr26tcHW8Qb/p5r2PZW0sTWyNza4A+uf1Xj1VTWzBy+S9H9kdW2YPpX5OF3s0sWkjEPIm/meSx5oeTRhn4Orqpw5q5KtkkY68ZLT4cfJekT7vA5cFCTdmJrTYZniVko1pnkO0N8apt2jDyuQT8AVxm1a6SeUukeXm5AJ/DwsAMl6bvVu5HC1zvAkc3EAmzeZyXmc57Nx0LvWx4gdNL+C0YlozZnsNRMwWcRnfjwTskF828dRqD6pNou0eqap38Dot2NJIySexuKskIDS93dBDWvJLW3/Byzt6IgqCRaSK/TC7LrkfRLO8c/mpskt0TUVsySlhd7jsDjoHYgD4d7NVbBY2Kui9rhhcAevnoqmeDA+3A5g8x+7rPngltD8D2WlGckzdJ0ZyTKyeTciRKy6hdZdEJO62Ch3W1CBLrEO62oQqdlFXrpLgLntnOV6wZBKyRtOwpmiE/RaKsfLZM0tSubKJqxPY1WTtaMTjYDlqeQ9VX1U81Rm4ljNLXu8t4XP3R4BD2rUHJrRcmx6WN7lZ9puAfVdv6fCse13/05n1Cd5aT6JxQNjGFgtz8eqnKNEE1IHX0W2zt55ro2jBs0PFQkAUiEnUteUJOiGpi2+RB5j6pWGV8Tscbi03BBaSCLcQRotuhcBllfVRjYSkSV9ounR6Duv7WZIiGVjDKw/8AdZYSN4d5uj+osfArq94vanQxxAwO+0SOF2sGJgb/AOQuHd6WJy4arxCqh7p8M/zSoKyyxKx8crou9rbeqKgunmfdzrsYBcNYzIuwDhqBfXXja1AWEp+qb3Yxb7gOltSXefvKDGJsY6FylsnTOsB0CZ7QJZug6D5LacuhbHTKFASEoEbSmmNsrbYAgsR48Ph+aHVjEwHi028nfqB6qEr7Z/vgpF4OY91ws7+E8+irPaaLQdNMLSHJMgpakGSOsHk6S6JXWiVpYVCxsKV1BbBRITWKOJYoQpKI5rpqZt2rmKdhBC6qiIDc+Sq+iIqK42KjTvN1m1jncIFDKs0o6H4dyonVVjsbhlhGpOpNtAhQ1Bz9Qo1o75/fBQiyK6mK1FI5Gd3OT/o5E4HUeqyWm5LTiP6ZopcW5eef5BPQkVDpGorK78QRi48vXJRLDy9B9SptEJiQO0v6IU1PY348kXBfLXqSf0Q9pksLSNLkG2mYBHyPqi+rZAcjMvgVTEWJHK4Vw1xdxFvRWWy9031AdUdrAyKMs7QSue0m7g2ws0jvEgC5Fy4JGT5GQKjaDe/a1rNY3S2YaAdfEFAIRKl2J7iOJvy1z04KbYbD9+Kuloqy33X2GypfhknbC0BuZvcg5EjLRts+o6hfa2zhDM+IPDw02Dm6OHA+irm1MjCMPA3HAg8wRmERtQ7MlhGpJJ4nU+Ki7I+gzQAhvk5ID6wcj6ArGzNOYPzHzVuS8Apkah3DxRYH5XaUhWS3Oqns51jnoUvlsPEuKaUHhYjXl4W+KKShROHBEWfIqkbsLuBu62VFYljiS2ohZdQBJYtXWKEEbAORm7RsbKmMxvdZI+6Wk6ouXFXM1wySFC7vW55JMPR6M2cCoo+GGMqdoYrJBiPUj0yW4u63EfJMPlgGdi52fqc1V19UXnkOAXQdRRyW3J2HhqjdXEDmlgIGuv78lyuJWFHXvAwgjXiP1UhkI4/Bc3XT7k7PopGzvq3ElrXBjA4gk2FjfQcdcss1x4fJ+Ieig58n8J8v1TJW1oC0OVLo2OeQ44MTgwn3iy5wE2+8RbRBqYZJIBNgwxYhhJ45uafi05C9uOoSX2ck3fmfH6BFjpjiybwucswBkT8R6KknIKo1EwjNWdFUARyMPbEuA7sOHC7C4SNMtwSGtcxpyskmafkuj3Lc/wC0FjQTjYbi18m2c4kcRa/zztYma0CL2cu1nePVTJ5ItRSuY97C0gtc5pDveBBIsfFAMZ4GyK6AY6VrfEpZzy5HFOBqbqRkA4KEBxwLdRZjbrHVR5Jepu5jieGiEmq0FdlcMymo0rEnWW1GZ4JMS7GKWUhzRzOY8PFWN0jCwMBe4i/y8E4x+IB3MBTNHSY3BLdErqSitgLOayTQnP7Nfa4zSTQVa0tU9mROSpOVK0JyTcVaFfsEn4VpXH21Yk+7L4M/3Evg86W1pYnm8wJimCXCbo9fEZi/MW/VFK3QU0k2+iD6ZAfCQmpGE94ZHiEpIX8Vqejm+dAnBEptbhDIPJSpn2d1yVF2WLeOpRRMCkZGcQotcU/kKaLBx8VKGoLLkcQWm+eRy+dkmyVM8EW7REFi+qa7Zzc2m3dc05Agtc0tc0g6gtLh5oTWAOdmMIJzytbhmPJZIr9xK+TTpXvc57zdzjcnS5KFISMwpDIeQPwQJnP94aclPASD3oBKiZDyWYv3dLbIYpVn+Ueo9FBr80erd/dHp9UH0WXZUsKMCl2lFDzwySYsuw7GD75sL6DU+Staea4PLh00VPDCSVa0kFnC5s05dOSu1caJGSjKxhpuj077HMK/jjgY0DInqq+tqYs7ALFfIa8/LVCklQAdFGeruFXzSEnJDdIqcLENWOfaHcysSnbLSHBg4FWtrLrE46hIMTNIMnc8OXqEs1yboRk48Ra3LxV8auSKZXUGDfKeFgTqEF7uYsjVOf3fQ/MJYv8AG45FaZHPQJ70Np4qTytsGRSWxkVZavZxGhQS/mE1RC7QDyUZaYg5ei015QkC1w5lZ9ptoVv7O92QbZPU2zWszfmfgiotktAqbFJm7Jgz6nkmInXPn4qFRUXyGnILcLslcqMzxYbD+Efv4pZrs0eoqC51zyt6afBJPKJA01LfMJQ0ruSJHK7gm5agtbpcoUiCQpeaXqiC0tHDP0RZJHu1FgslYAxx8D+SXLfRZdlMiMcUNSbdZ0OG4mu4KxpIBx7x4a5KrZJb93Tkcj7AnFbwH5aeadHoU0OGodzUTITxUSb5g3HBFMVm3usjVM16oYewCPEquR4KnNVEi3BAD1UTxD3CxDusQoPEDLHmUIhWU1PlkkMGaszaiBT1G7uEWJJOnwy5JUxlM0VMXNJBIztrbPL80zF+wrO/wByROHMdRf5JeR44hNy0JHG/ndIzQuHD5JzkY0gLszkjMHBChZc26n0F1Z0FIJHWvb9El7ZoxtK7N9l3RmR0W2TyN44uqd7Gwtyy9EOOH15/1WuvKMl7C09dfVEkIdzslZaXkfhZThJGRV1J9MDNmNgUC7ktSsULIENyS5+vyCgJWjUKLhp5rUbLqqYWMR1I4N81vthfNSsAEtIrNsA4HtI010/TmgVwysOKhE7ujXiPifyUjop2qIUkjbFaHJdzufsiOYTYxfCWEdHA/wD5K5Cphwyvba2F7h0sSFi5/m4rwanjaxxn8kqaHmVc0zW2BBIyHG/DxVdTRKyLMm5ZXYD0uLrVhM0x2koI5Guta4J93S/Hoqyohc04Su13q2zHO2J0cYZKwFshaMLXDLBYeAvquXqZ7tzGaxZL9xjYy0U5jWsAU3tOqgHoIbFpmsKxbusRL0OVb7OshUVGZZAxuriAOpT9LupVzBzw0WaLm7gCegVrujsCaKtp3vAwiQXz8ClzmkrNsfTZavi9HX03sdBjaXzuD8sVgLDmBcLlt9N3GUU3ZRuu0i/HECGsJxfzi36L6Jij7q8Y9tLA2tY42A7BvUuL5AD492O3kEPSSnz/ACfaOdmnyVHms1MOZSMtO4aOV/R0j57iGOSXh/dsc8DqQLBMTbqV1v8Ao57c8B0WxtCkmclTueHWBti7h8Q4i4PguwpN32tIN7rmZGdlMztGubhe0ua4FpsHC+R8Lr6X2huZTSMDWxhttHMyNuoWL1OSUK4qzVg9v/M8Dqow2RzRwP0CBJGFa717PFPWzQglwY4C51za12fqqvHbULq4ncFZiyVyddGom2Plz5/0QKmfOwRr5m2mQ+Z+qVkGau+ihIG60WqIaiNb4eaqEFhyHVEiZZFwiw6/VZfJBIjYN+iFa6m6RYHokAjIG3P9fqpxu1TuwNmfaZuyxiO9yXFuLRugFxckgDUa3QtpbOdBPJC44jG97CRkDhcW3HWyqnug15LrdSpwGT+IM+GL81RbZgc+se1jS5z3DC0C5JcAfzTNDK5rxbiLLo9zKf8Axj5Hi7iGhp5Djb0CxZFxzN/KOhD8/TqPwyz3d9k0sjQ6eoEV/uRtxuHV7rC/krfaHsiDYyYKqRzxmGyhmBxGdjhaCF6Nsgd0J2Uqc5Lpi3CKdUfNBke10sUjcMkZ74PAg2+oVfJOL2K67e6kB2hVSAXxPA6BrWtPxBVI7ZYJuWpeTLylbD9m60UFXG7XOyHHTm111UlJcBuFaGy7i2E+iopqgr0s0czgWLpv7E/hPosR5ot9vM7TZlC5xwCW1xnnYZK+2Zuqxr2ySTYsJBABsLhV7YKYZ9sB0KMIKXjUH+crIoyXg6+T12Sarl3/AA79u0WDLEPULi/aHu4yumpZWPitE4NmY91g+HFis0gHMHELZe8hgUP/AMt/95UX1FAPvj+Yq0ZTi9HN9iD2d3suNjIwyNoa1os1rRYAcLAItRoqbdzaUcoIjdiDe70yyV+9twtUXaESVSPIPaFs1kgIeMufEeIXptPt6na1rRI0gNa0EkXIAABK5HfSjLg7Lh8kjs3btAGMxx2Ia2/dBzsLqs+WuIzhB/scPv1MJNoVL25gvy8mtH0SVDsmN7XufKWubE6RoAv3g7C1pzyF8vNQ3nqmvqZnx5MdK8t4d2+WSqnVBAJvoD6rqJfglZzn+zJSu16/IAH5FKgLWI5XOdvjxUgUbsqaBKKxyGpxDNFECtPwI+RUZE6acCHHx7QN/wDqSPkfRISnVHwQXLlFpN0TCENxCqQapZiyRrwSL5crHgmJJiSSbZ+Z9VXNflYozX3HjoeqidEY1S1AjmjeQCGuBcOYvmPRdxsHeCmqKljIo8DsyTlmMsl50w6q83GewbQga3Vzi0/yk/RZfUQTakavT5HFOK8n0bsxtmhTrZA1pcdACT0AuUSlbZoXOe0asMWz6p4Nj2L2g+LxgFvVJrQy9nk1R7QoC5zuxvck353JN1Xy79svdsQXGMiPKyk6nCr7MS/3GQ62Df8Asf8AKBU5vaM8+7E0LjvswW2U44Ke3EDzZDqf/cGX8DVi5j7IeS2p7cCn3MgmF/GU+q2L8ZT6oVU1VpT+K+BPJ/Jdtc3jKfUqQkj4yH1KolgU18Et/J6j7Ld4ooKsRY+7PhYbnLGL4D4akeYXv0LrhfGUchaQQbEEEEagjMEL6n3B28KujhmyxFoa+3B7cn/EX8wly7sZHaoZ3lprsJsvDNsbaZCXRlpJaSPyX0PtGK7T0XzP7SKfBVu5EA+YuD8grQdBmrjYCeTG0Otr3s+F0nKcgD+IX+f0TlO28bB/C35BDnpnDMgW5+RW2nRk8k2NbbS6WmepRutlewPFalgI/NHsBEIrCLhBsVNqhBh1QbYeGvDgMvmUs45orCM+mXrZLG9/JAhqVyCjdkXFE+xHmPNCmyWKOejQu5hORUoHitmmYDf65I8GGwDWuTe6dS2CvjqHhxjidikLRiIDmljbDiS5wCPsyhkqZWwU0eOR3AaNHFzjwA5r1/db2UQQxj7W4zuLhI5jbthEgaQ22jnYcTrXI1vbRKzVVF8d3Z0lFvTEWBz45Y2nRz2tIPkxxI9FzHtjrmO2U90bw4PkhaC03Hv4rZf6TkrDe7dQGIup5ZI3NGTC4uiIHAA3LT4/BV2zKIsp2xktkE0nfcBk6JuFuEgi4zL7jw63S+FaGrne/J4BHU2yKZDwUtWwgOOHS5t0vl8FlOCFUsnTpjCnACCtRjNORsScjo0QVoztjyWIvZLEnkX9tfAvLQPtoqWSM3txXX7R2s0MNhmVzdBKA4uOZ4LXzbMrxpOhFzbZLTQn6uAtOIjVKnVXFgivVvYTtwtmkpHHuvHas8Htwhw822P+1eXsivdWu520vs1dBMTYNkaHG9gGO7rr9ASfJVa0GLpn1k4Ym2Xz/wC1vZ4fMJYLvjY20hDXDCXEFhOIC7Tc2IuMivZxtsNcxrbFzgSb6NBBa3D+JxeW5crlW9PTsGI2BLjdxsMz+XghEvJeD5WoZxhb0A9P6Jk1LNCbr3zfDcelq43ERMZNbuSNaGm40D7e83rzysvn6elw3Dm4XA4XAgXBBsR6rVHL4M8sfknI+MjK10pDVjQjLhmoGEch6IRjbyCLyMrxHHStPghi10EsHALMCtbAGc3MW4An0z+iA6QAqbW8tf6/khmO6FuyAu2Ol0YMeeKmyEN6qbdf30+qlaIaER4vPlkttpQdQT1JKMArrdnYUlZOyBlxize4fcYPed1zFvEjxS7L0et+yLdxtJRCVzQJaiz3HiI8+yZ/Kbkc3Hku3e/LVUe26vs2w08XdLi2NvNrWjM+TQfRGqpMOGNt8hbyHNIbNccXRHas3dIvqvH9tb6CmZLRwtLpRjZ2uPJnaA9phFveGg4A3PBehbybVEMT5DmWtJaObtGj1svn+Glc95JN3EkuPEkm5Pql8ki84ukkLTDJNSxYS08wj1OzHBpPJE2jFeKN3IJkGmhGSLT2Is1TkZzUKSjc8XCabSOBWfK1dGrEnRtbU+yKxJHnOOnLkGm94dVqE6rUR7w6relRzJO3ZdbYeDhHIKpcMwmtovNx0QKUYjmrSZWKslBoUEDNNloGiWCBGezbp7QOGiklc5wuYtCA0tjtGSfvE6L1WlqR6rxnZc5bsgSNAxRkOaSL2N/2F3m6u246mFsjDYaEE5tcNWu8UucadmjE1JcTrhWAPwO43wngbajqvIva1uuYpTVxtvHIR2tvuSZAHo7Lz6r0zbLC+AljgHgYmO5ObmP1SGwN46Supn9q+NvdLJ43uaMJsQ4Oufd1sdCjGWwTguJ88vYhYPoibUrY2TzMiJfG2WRsb9ccbXkMdfjdoBSgrxxB9E67MrsK8fVYSo/aYyfeI11C0KiL8R9D+SfyXyUphojYi/Vadqf3qof2jHycfIDNafWN4h2dtBa1srfD4oclYaZIogGSX+3D7rPUrDO48cPT8yo5oiiyzjw2uTZd37Md8dnUQn+0PLZHloa4Mc8FjQe73QbG5J8xyXmJw8QT1KwnKwbYeASmXTpnsG629TNo7Xc9vdiiicIA7Jz7luN5HOzR42C7Ss2iC4tbkfjbS55dF820lQ+F7ZI3Fj2nE1wyIK9j3F3ogq2ODjgqBd8t7WLRq+PmBpbUJMotGvDlT7KD2k7cDHtpuNsb/PJg/wCR9FwIqsDibapbb21HVFTJMfvvJHg0d1g8mgIe0L4QUtxVkeVvfwO1G18TS1HrJP7iPxXNBX9S7/Dx9U2EUk6EzyOTTYOnryzIKR2k5xCU4puIC4SMqSdj8UpNVY12rlimsSLNFf05qBuqgz3h1RKXioWzC3nNLGX6INC3vFTqXZjopUOZKE+gw7BznNLu0Uqp3eKJTxYiAiuiS7PStn5bFd++KotxN4xS1BZI60MtmuP4X/df9D5cLq9E7BslzMQvpZeaVWqtJaoEJOLtH0jJtQU0UkspJiawuxDO9hkPAnLrdfO9bIZHOcdXEl1tLk3suk2lvg6TZkFHe7g7+9vxbER2Q88if9HiuZYVSEaGZsnJ6BxRWRuzC2ApBNoSD7IKJhRwFIBSiCb4Vt6bLFoahvL4DmpRAEUSYZEjRR3ztki4QNUaABbGOSlgPgFIzgaBCeZHaCyJBaoYSfDmUOneWuu04TmLg2NiCDmPAkeaLNCGi17uOvFCOSXIKJVMQysFufNqiHG2eqi/RJ4jsfQnAM1cvzpx4FJbI2e+Z+FtvNdVDutJgwOPoj7kY6bCsUpK0jlXahORcF0Y3OHF59E1FutGPvFJyZIyQ7HhnHZQWWLq/wD02z8SxZ7NNHl9JxWnajqtLF0TljVbwRNm8VixCfQYfsKVfvFO7M18isWIxJLsvz/0r/8AUucl95YsV5FEaHDzRAtrEERk1gWLFYhNqkFixEBtQ4u6BbWKEGo9PJQesWKyIbi1TZ0WLFCFXJqhj3lpYqMKByalacsWJMux0Oh/dX/NK9Dp1ixYs37m3036lpApLFiohrCLFixEh//Z',
        },
        {
          cat_name: 'Electrnics',
          cat_image:
            'https://www.ethiopianimporter.com/contentimageexport/Electronics.jpg',
        },
      ],
      topDeals: [
        {
          image:
            'https://www.pocket-lint.com/r/s/660x/assets/images/142227-phones-review-iphone-x-review-photos-image1-ahdsiyvum0-jpg.webp',
          name: 'Iphone X',
        },
        {
          image:
            'https://m.jagranjosh.com/imported/images/E/Articles/skin%20products%20feature.jpg',
          name: 'Product',
        },
        {
          image:
            'https://m.jagranjosh.com/imported/images/E/Articles/skin%20products%20feature.jpg',
          name: 'Women Beauty Products',
        },
        {
          image:
            'https://ae01.alicdn.com/kf/H56757d23988149e9a0ba0b31cedd9501V/2020-Trend-African-Men-Clothes-Male-Long-SleevePatchwork-Dashiki-Long-Top-African-Print-Causal-Wedding-African.jpg_q50.jpg',
          name: 'Mens Clothes',
        },
      ],
    };
  }

  sendLocation = () => {
    var location_id = this.state.locationName;

    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch('http://vmi317167.contaboserver.net/ecommerce/api/stores_list', {
          method: 'POST',
          headers: {
            Accept: 'application/json',
            'Content-Type': 'application/x-www-form-urlencoded',
          },
          body: 'location_id=' + location_id,
        })
          .then(response => response.json())
          .then(result => {
            this.setState({isLoading: true});
            setTimeout(() => {
              this.setState({isVisiable: false});
            }, 4500);

            AsyncStorage.setItem('selected_location_id', location_id);
            this.setState({selected_shops: result});
          });
      } else {
        alert('Please Check Your Internet Connection');
      }
    });
  };

  componentWillMount() {
    setTimeout(() => {
      this.setState({isVisiable: true});
    }, 1500);
    this.getStores();
  }

  //  get store name //

  getStores = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/shop/locations',
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            if (result.flag) {
              // this.setState({isLoading: true});
              for (let i = 0; i < result.data.length; i++) {
                const element = result.data[i];
                // storesLocation.push(element);

                const stores = {
                  id: element.id,
                  name: element.area_name,
                };
                storesLocation.push(stores);
              }
            }
          });
      } else {
        alert('No Inter net Connection');
      }
    });
  };
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Header
          placement="left"
          leftComponent={
            <IconButton
              icon="menu"
              onPress={() => this.props.navigation.openDrawer()}
              color={Colors.blue800}
              size={20}
            />
          }
          // centerComponent={{text: 'MY TITLE', style: {color:'blue'}}}
          rightComponent={
            <TouchableOpacity
              style={{flexDirection: 'row', marginRight: 8}}
              onPress={() => console.log('Press')}>
              <Icon name="search" size={20} color={Colors.blue800} />
              <TouchableOpacity style={{marginLeft: 10}}>
                <Icon name="shopping-cart" size={20} color={Colors.blue800} />
              </TouchableOpacity>
            </TouchableOpacity>
          }
          containerStyle={{
            backgroundColor: '#fff',
            justifyContent: 'space-around',
          }}
        />

        <ScrollView>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.state.bannerImage}
            renderItem={items => {
              return (
                <Image
                  source={{uri: items.item.image}}
                  style={{
                    height: 200,
                    width: Dimensions.get('window').width,
                  }}
                />
              );
            }}
            keyExtractor={items => items.id}
          />
          <Text
            style={{
              paddingTop: 20,
              alignItems: 'center',
              textAlign: 'center',
              height: 60,
            }}>
            All Categories
          </Text>
          <FlatList
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.state.productCategory}
            renderItem={items => {
              return (
                <View>
                  <Image
                    source={{uri: items.item.cat_image}}
                    style={{
                      height: 60,
                      width: 60,
                      borderRadius: 60,
                      marginLeft: 5,
                      justifyContent: 'space-around',
                    }}
                  />
                  <Text
                    style={{
                      fontSize: 10,
                      alignItems: 'center',
                      textAlign: 'center',
                    }}>
                    {items.item.cat_name}
                  </Text>
                </View>
              );
            }}
            keyExtractor={items => items.id}
            // style={{paddingTop: 20, width: Dimensions.get('window').width}}
          />

          <Text style={{paddingTop: 20, marginBottom: 20, textAlign: 'center'}}>
            Top Deals
          </Text>
          <FlatList
            style={{width: Dimensions.get('window').width}}
            horizontal
            showsHorizontalScrollIndicator={false}
            data={this.state.topDeals}
            renderItem={items => {
              return (
                <View>
                  <Image
                    source={{uri: items.item.image}}
                    style={{
                      height: 200,
                      width: 200,
                      justifyContent: 'space-around',
                      marginLeft: 9,
                    }}
                  />
                  <Text style={{textAlign: 'center', paddingTop: 10}}>
                    {items.item.name}
                  </Text>
                </View>
              );
            }}
            keyExtractor={items => items.id}
            // style={{width: Dimensions.get('window').width}}
          />
          {this.state.selected_shops != null ? (
            <View>
              <Text
                style={{marginTop: 40, marginBottom: 30, textAlign: 'center'}}>
                Shops Selected Area's
              </Text>
            </View>
          ) : (
            <View>
              <Text style={{fontSize: 16, textAlign: 'center'}}>
                No Shops Found
              </Text>
            </View>
          )}

          {this.state.selected_shops != null ? (
            <FlatList
              data={this.state.selected_shops}
              style={{width: Dimensions.get('window').width, height: 400}}
              renderItem={(items, index) => {
                console.log(items.item.vendorInfo);
                console.log(items.item.vend);
                return (
                  <TouchableOpacity
                    onPress={() =>
                      this.props.navigation.replace('Products', {
                        value: items.item.vendorInfo.id,
                      })
                    }>
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={{
                          uri:
                            'https://rimh2.domain.com.au/5m0P0sF5FxFgxoG9pJB4DUMWZDo=/fit-in/800x600/filters:format(jpeg)/2015232523_1_1_190503_025710-w847-h565',
                        }}
                        style={{height: 100, width: 150}}
                      />
                      <View>
                        <Text
                          style={{
                            marginTop: 4,
                            marginLeft: 10,
                            fontSize: 20,
                            fontWeight: 'bold',
                          }}>
                          {items.item.vendorInfo.shop_name}
                        </Text>
                        <Text
                          style={{
                            paddingLeft: 10,
                            marginTop: 14,
                            color: '#9e9e9e',
                          }}>
                          Shop Open Time:{' '}
                          {items.item.vendorInfo.shop_opening_time}
                        </Text>
                        <View flexDirection="row">
                          <Text
                            style={{
                              marginTop: 14,
                              paddingLeft: 10,
                              fontSize: 16,
                            }}>
                            ShopAddress :
                          </Text>
                          <View>
                            <Text
                              style={{
                                paddingLeft: 10,
                                marginTop: 16,
                                fontSize: 13,
                                color: '#9e9e9e',
                              }}>
                              {items.item.vendorInfo.shop_address}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                );
              }}
              keyExtractor={items => items.id}
            />
          ) : (
            <View>
              <Text style={{textAlign: 'center'}}>No Shops Found</Text>
            </View>
          )}
        </ScrollView>

        <Modal isVisible={this.state.isVisiable}>
          <View
            style={{
              flex: 1,
              justifyContent: 'center',
              alignItems: 'center',
              color: '',
            }}>
            <Card title="SELECT LOCATION">
              <SearchableDropdown
                onTextChange={text => console.log(text)}
                onItemSelect={item => {
                  this.setState({locationName: item.id});
                }}
                containerStyle={{padding: 5}}
                itemStyle={{
                  padding: 10,
                  marginTop: 2,
                  backgroundColor: '#ddd',
                  borderColor: '#bbb',
                  borderWidth: 1,
                  borderRadius: 5,
                }}
                itemTextStyle={{color: '#222'}}
                itemsContainerStyle={{maxHeight: 140}}
                items={storesLocation}
                defaultIndex={2}
                resetValue={false}
                textInputProps={{
                  placeholder: 'placeholder',
                  underlineColorAndroid: 'transparent',
                  style: {
                    padding: 12,
                    borderWidth: 1,
                    borderColor: '#ccc',
                    borderRadius: 5,
                    width: 300,
                  },
                  onTextChange: text => alert(text),
                }}
                listProps={{
                  nestedScrollEnabled: true,
                }}
              />
              {this.state.isLoading ? (
                <Button title="Loading button" loading />
              ) : (
                <Button title="Loading button" onPress={this.sendLocation} />
              )}
            </Card>
          </View>
        </Modal>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
  