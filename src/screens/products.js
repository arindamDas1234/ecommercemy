import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  Image,
  FlatList,
  Dimensions,
} from 'react-native';

import {Header, Card, Button} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';

import {IconButton, Colors} from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

export default class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      vendor_category: [],
    };
  }

  componentWillMount() {
    this.productsGet();
    this.vendorCategory();
  }

  productsGet = () => {
    var id = this.props.route.params.value;
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/vendor/products?vendor_id=' +
            id,
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            console.log(result.flag);
            if (result.flag) {
              this.setState({products: result.data});
            } else {
              alert('No Products Found');
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        alert('Please Check YOur Internet Connection');
      }
    });
  };

  vendorCategory = () => {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/vendor/categories?vendor_id=' +
            this.props.route.params.value,
          {
            method: 'GET',
            headers: {
              'Content-Type': 'applicationjson',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            if (result.flag) {
              this.setState({vendor_category: result.data});
            } else {
              alert('No Vendor Categoryb Found');
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        alert('Please Check Your Internet Connection ');
      }
    });
  };

  productDetails = (data, id) => {
    this.props.navigation.replace('ProductDetails', {
      value: data,
      result: id,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />
        <Header
          placement="left"
          leftComponent={
            <TouchableOpacity
              onPress={() => this.props.navigation.replace('Home')}>
              <Icon
                name="angle-left"
                size={28}
                containerStyle={{
                  paddingRight: 10,
                }}
                color={Colors.blue800}
              />
            </TouchableOpacity>
          }
          // centerComponent={{text: 'MY TITLE', style: {color:'blue'}}}
          rightComponent={
            <TouchableOpacity
              style={{flexDirection: 'row', marginRight: 8}}
              onPress={() => console.log('Press')}>
              <Icon name="search" size={20} color={Colors.blue800} />
              <TouchableOpacity style={{marginLeft: 10}}>
                <Icon name="shopping-cart" size={20} color={Colors.blue800} />
              </TouchableOpacity>
            </TouchableOpacity>
          }
          containerStyle={{
            backgroundColor: '#fff',
            justifyContent: 'space-around',
          }}
        />
        <FlatList
          style={{
            width: Dimensions.get('window').width,
            marginTop: 10,
            marginLeft: 10,
          }}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={this.state.vendor_category}
          renderItem={(items, index) => {
            if (items.item) {
              return (
                <TouchableOpacity>
                  <View>
                    <Image
                      source={{uri: items.item.category.image_active}}
                      style={{height: 50, width: 50, borderRadius: 100}}
                    />
                    <Text style={{textAlign: 'center'}}>
                      {items.item.category.name_en}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            } else {
              return (
                <View>
                  <Text
                    style={{
                      textAlign: 'center',
                      fontSize: 16,
                      justifyContent: 'space-around',
                    }}>
                    No Category Found
                  </Text>
                </View>
              );
            }
          }}
        />

        <FlatList
          data={this.state.products.data}
          horizontal={false}
          numColumns={3}
          style={{
            flexDirection: 'column',
            width: Dimensions.get('window').width,
          }}
          renderItem={(items, index) => {
            console.log(items.item.image.medium);
            if (items.item) {
              //   console.log(items.item.prices[0].text);
              return (
                <TouchableOpacity
                  onPress={() =>
                    this.productDetails(
                      items.item.slug,
                      items.item.prices[0].vendor_id,
                    )
                  }>
                  <View>
                    <Image
                      source={{uri: items.item.image.medium}}
                      style={{
                        height: 100,
                        width: 100,
                        justifyContent: 'space-around',
                        marginVertical: 30,
                        marginHorizontal: 10,
                      }}
                    />
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 16,
                        fontWeight: 'bold',
                      }}>
                      {items.item.title_en}
                    </Text>
                    <Text style={{textAlign: 'center'}}>
                      Price : ₹ {items.item.prices[0].total}/{' '}
                      {items.item.prices[0].size}
                    </Text>
                  </View>
                </TouchableOpacity>
              );
            } else {
              return (
                <View>
                  <Text style={{textAlign: 'center', fontSize: 16}}>
                    NO Products Found
                  </Text>
                </View>
              );
            }
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
