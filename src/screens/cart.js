import React, {Component} from 'react';

import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  StatusBar,
  FlatList,
} from 'react-native';

import {Header, Card, Button} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';

import {IconButton, Colors} from 'react-native-paper';

export default class Cart extends Component {
  render() {
    return (
      <View style={styles.container}>
        <StatusBar backgroundColor="#fff" barStyle="dark-content" />

        <Header
          containerStyle={{
            backgroundColor: '#fff',
          }}
          leftComponent={
            <IconButton
              icon="menu"
              onPress={() => this.props.navigation.openDrawer()}
              size={20}
              color={Colors.blue800}
            />
          }
          centerComponent={{
            text: 'Shopping Cart',
            style: {color: 'black', fontSize: 20},
          }}
          rightComponent={
            <TouchableOpacity
              style={{flexDirection: 'row', marginRight: 8}}
              onPress={() => console.log('Press')}>
              <Icon name="search" size={20} color={Colors.blue800} />
              <TouchableOpacity style={{marginLeft: 10}}>
                <Icon name="shopping-cart" size={20} color={Colors.blue800} />
              </TouchableOpacity>
            </TouchableOpacity>
          }
        />

        <View
          style={{
            flexDirection: 'row',
            backgroundColor: '#ffeb3b',
            marginTop: 5,
            height: 45,
            width: 340,
          }}>
          <Text
            style={{
              textAlign: 'center',
              fontSize: 18,
              marginLeft: 50,
              marginTop: 10,
              fontWeight: 'bold',
            }}>
            Total Items
          </Text>
          <View>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
                marginLeft: 70,
                marginTop: 10,
                fontWeight: 'bold',
              }}>
              Total Price
            </Text>
          </View>
        </View>

        <FlatList />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex:1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
