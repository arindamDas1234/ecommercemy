import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Image,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  AsyncStorage,
} from 'react-native';

import {
  Header,
  Card,
  Button,
  Rating,
  AirbnbRating,
  Avatar,
} from 'react-native-elements';

import Icon from 'react-native-vector-icons/FontAwesome';

import {IconButton, Colors} from 'react-native-paper';

import NetInfo from '@react-native-community/netinfo';

import ImageSlider from 'react-native-image-slider';
import Cart from './cart';

export default class ProductDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      vendor_id: '',
      products_details: [],
      product_image: '',
      product_price: [],
      price_id: '',
    };
  }

  componentWillMount() {
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/vendor/product/slug/' +
            this.props.route.params.value,
          {
            method: 'GET',
            headers: {
              'Content-Type': 'application/json',
            },
          },
        )
          .then(response => response.json())
          .then(result => {
            if (result) {
              this.setState({products_details: result});
              this.setState({product_image: result.image.large});
              this.setState({product_price: result.prices[0]});
              this.setState({vendor_id: result.prices[0].vendor_id});
              this.setState({price_id: result.prices[0].id});
            } else {
              alert('No Products Found');
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        alert('Please Check Your Internet Connection');
      }
    });
  }

  productRatting = ratting => {
    console.log('The Ratting is ' + ratting);
  };

  checkLogin = async () => {
    await AsyncStorage.getItem('user_login').then(result => {
      if (result) {
        fetch('http://vmi317167.contaboserver.net/ecommerce/api/cart/add', {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
            Accept: 'application/json',
          },
          body:
            'product_price_id=' +
            this.state.price_id +
            '&quantity=' +
            3 +
            '&vendor_id=' +
            this.state.vendor_id +
            '&user_id=' +
            result,
        })
          .then(response => response.json())
          .then(res => {
            console.log(res.data);
            this.props.navigation.navigate('Home', {screen: 'Cart'});
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        this.props.navigation.navigate('Login', {
          value: this.props.route.params.value,
        });
      }
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <StatusBar backgroundColor="#fff" barStyle="dark-content" />
          <Header
            placement="left"
            leftComponent={
              <TouchableOpacity
                onPress={() =>
                  this.props.navigation.replace('Products', {
                    value: this.props.route.params.result,
                  })
                }>
                <Icon
                  name="angle-left"
                  size={28}
                  containerStyle={{
                    paddingRight: 10,
                  }}
                  color={Colors.blue800}
                />
              </TouchableOpacity>
            }
            // centerComponent={{text: 'MY TITLE', style: {color:'blue'}}}
            rightComponent={
              <TouchableOpacity
                style={{flexDirection: 'row', marginRight: 8}}
                onPress={() => console.log('Press')}>
                <Icon name="search" size={20} color={Colors.blue800} />
                <TouchableOpacity style={{marginLeft: 10}}>
                  <Icon name="shopping-cart" size={20} color={Colors.blue800} />
                </TouchableOpacity>
              </TouchableOpacity>
            }
            containerStyle={{
              backgroundColor: '#fff',
              justifyContent: 'space-around',
            }}
          />

          <Image
            source={{uri: this.state.product_image}}
            style={{
              height: 300,
              width: Dimensions.get('window').width,
              borderBottomWidth: 1,
            }}
          />
          <View style={{marginTop: 15}}>
            <Text
              style={{
                fontSize: 20,
                fontWeight: 'bold',
                marginTop: 10,
                marginLeft: 10,
              }}>
              {this.state.products_details.title_en}
            </Text>
            <Text style={{fontSize: 16, marginLeft: 15, marginTop: 13}}>
              $ {this.state.product_price.total} /
              {this.state.product_price.unit}
            </Text>
            <View style={{marginLeft: 85}} />
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginTop: 24,
              justifyContent: 'space-around',
            }}>
            <Button
              title="ADD TO CART"
              type="outline"
              onPress={this.checkLogin}
              containerStyle={{
                width: 150,
              }}
            />
            <View>
              <Button
                title="BUY NOW"
                containerStyle={{
                  width: 150,
                }}
              />
            </View>
          </View>
          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              marginTop: 20,
              marginLeft: 10,
            }}>
            Products Description
          </Text>
          <Text style={{fontSize: 14, marginLeft: 12, marginTop: 20}}>
            {this.state.products_details.description_en}
          </Text>

          <Text
            style={{
              fontSize: 20,
              fontWeight: 'bold',
              marginTop: 30,
              marginLeft: 8,
            }}>
            Customer Review
          </Text>
          <View style={{flexDirection: 'row'}}>
            <Avatar
              size={60}
              containerStyle={{
                marginTop: 20,
                marginLeft: 10,
              }}
              rounded
              source={{
                uri:
                  'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
              }}
            />
            <View>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginTop: 25,
                  marginLeft: 10,
                }}>
                Arindam Das
              </Text>
              <Rating imageSize={15} readonly />
            </View>
          </View>
          <Text style={{fontSize: 14, marginLeft: 12}}>
            I'm a Frontend Engineer with a passion for web technologies and
            making the web accessible to everyone.{' '}
          </Text>

          <View style={{flexDirection: 'row'}}>
            <Avatar
              size={60}
              containerStyle={{
                marginTop: 20,
                marginLeft: 10,
              }}
              rounded
              source={{
                uri:
                  'https://s3.amazonaws.com/uifaces/faces/twitter/ladylexy/128.jpg',
              }}
            />
            <View>
              <Text
                style={{
                  fontSize: 16,
                  fontWeight: 'bold',
                  marginTop: 25,
                  marginLeft: 10,
                }}>
                Arindam Das
              </Text>
              <Rating imageSize={15} readonly />
            </View>
          </View>
          <Text style={{fontSize: 14, marginLeft: 12}}>
            I'm a Frontend Engineer with a passion for web technologies and
            making the web accessible to everyone.{' '}
          </Text>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});
