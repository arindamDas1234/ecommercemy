import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  TouchableOpacity,
  AsyncStorage,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {Input, Card, Button} from 'react-native-elements';
import {IconButton, Colors} from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';

export default class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isLoading: false,
      passwordVisiable: true,
      password: '',
      email: '',
    };
  }

  passwordShow = () => {
    this.setState({passwordVisiable: false});
  };

  hidePassword = () => {
    this.setState({passwordVisiable: true});
  };

  login = () => {
    console.log(this.state.email);
    NetInfo.fetch().then(status => {
      if (status.isConnected) {
        fetch(
          'http://vmi317167.contaboserver.net/ecommerce/api/customer_login',
          {
            method: 'POST',
            headers: {
              Accept: 'application/json',
              'Content-Type': 'application/x-www-form-urlencoded',
            },
            body:
              'username=' +
              this.state.email +
              '&password=' +
              this.state.password,
          },
        )
          .then(response => response.json())
          .then(result => {
            if (result.flag) {
              this.props.navigation.replace('ProductDetails', {
                value: this.props.route.params.value,
                data: result.data.id,
              });
              AsyncStorage.setItem('user_login', result.data.id.toString());
            } else {
              alert('Some Thing Went Wrong');
            }
          })
          .catch(error => {
            console.log(error);
          });
      } else {
        alert('Please Check Your Enternet Connection');
      }
    });
  };

  // registerPath = () => {
  //   this.props.navigation.navigate('Register');
  // };

  render() {
    return (
      <View style={styles.container}>
        <Card
          title="USER LOGIN"
          containerStyle={{
            marginTop: 100,
          }}>
          <Input
            placeholder="USER EMAIL ADDRESS"
            label="User Email"
            onChangeText={value => this.setState({email: value})}
            leftIcon={<Icon name="user-o" size={20} color={Colors.grey500} />}
            containerStyle={{
              marginTop: 50,
            }}
          />
          <Input
            placeholder="USER PASSWORD"
            label="Password"
            secureTextEntry={this.state.passwordVisiable}
            onChangeText={value => this.setState({password: value})}
            leftIcon={
              this.state.passwordVisiable ? (
                <Icon
                  name="eye"
                  size={20}
                  color={Colors.grey500}
                  onPress={this.passwordShow}
                />
              ) : (
                <Icon
                  name="eye-slash"
                  size={20}
                  color={Colors.grey500}
                  onPress={this.hidePassword}
                />
              )
            }
          />
          {this.state.isLoading ? (
            <Button loading />
          ) : (
            <Button title="LOGIN" onPress={this.login} />
          )}
          <TouchableOpacity>
            <Text
              style={{
                textAlign: 'center',
                marginTop: 40,
                fontSize: 14,
                color: '#0091ea',
              }}>
              Forgot Your Password ?
            </Text>
          </TouchableOpacity>

          <Text style={{textAlign: 'center', fontSize: 14, marginTop: 14}}>
            Don't have Account
          </Text>
          <TouchableOpacity
            onPress={() => {
              this.props.navigation.replace('Register', {
                value: this.props.route.params.value,
              });
            }}>
            <Text
              style={{
                textAlign: 'center',
                fontSize: 18,
                color: '#2196f3',
                marginTop: 10,
              }}>
              Register
            </Text>
          </TouchableOpacity>
        </Card>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
});
