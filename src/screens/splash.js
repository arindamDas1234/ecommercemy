import React, {Component} from 'react';

import {StyleSheet, Text, View, TouchableOpacity, Image} from 'react-native';

export default class Splash extends Component {
  constructor(props) {
    super(props);
  }

  componentWillMount() {
    setTimeout(() => {
      this.props.navigation.replace('Home');
    }, 3500);
  }

  render() {
    return (
      <View style={styles.container}>
        <Image
          source={require('../Images/Logo.jpg')}
          style={{height: 200, width: 300}}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FEFDFD',
  },
});
