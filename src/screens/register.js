import React, {Component} from 'react';

import {
  StyleSheet,
  View,
  Text,
  Touchableopacity,
  StatusBar,
  Dimensions,
  ScrollView,
  KeyboardAvoidingView,
  Picker,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';
import {Input, Card, Button} from 'react-native-elements';
import {IconButton, Colors} from 'react-native-paper';
import NetInfo from '@react-native-community/netinfo';

import DatePicker from 'react-native-datepicker';

export default class Register extends Component {
  constructor(props) {
    super(props);

    this.state = {
      gender: '',
      date: '',
      first_name: '',
      last_name: '',
      email: '',
      password: '',
      confirmPassword: '',
      number: '',
      password_visiable: true,
      first_name_validation: true,
      last_name_validation: true,
      email_validation: true,
      password_validation: true,
      confirmPassword_validation: true,
      number_validation: true,
      isLoading:false
    };
  }

  show = value => {
    this.setState({gender: value});
  };

  showPassword = () => {
    this.setState({password_visiable: false});
  };

  hidePassword = () => {
    this.setState({password_visiable: true});
  };
  showCpassword = () => {
    this.setState({password_visiable: false});
  };
  hideCpassword = () => {
    this.setState({password_visiable: true});
  };

register = ()=>{

  if(this.state.first_name ==""){
    this.setState({first_name_validation:false})
  }else if(this.state.last_name ==""){
    this.setState({last_name_validation:false})
  }else if(this.state.email ==""){
    this.setState({email_validation:false})
  }else if(this.state.password == ""){
    this.setState({password_validation:false});
  }else if(this.state.confirmPassword ==""){
    this.setState({confirmPassword_validation:false});
  }else if(this.state.number ==""){
    this.setState({number_validation:false})
  }else{
    NetInfo.fetch().then(status =>{
        if(status.isConnected){
            fetch('http://vmi317167.contaboserver.net/ecommerce/api/customer_register',{
              method:'POST',
              headers:{
                Accept:'application/json',
                'Content-Type':'application/x-www-form-urlencoded'
              },
              body:'first_name='+this.state.first_name+
                  '&last_name='+this.state.last_name+
                  '&email='+this.state.email+
                  '&password='+this.state.password+
                  '&password_confirmation='+this.state.confirmPassword+
                  '&gender='+this.state.gender+
                  '&dob='+this.state.date+
                  '&receive_offer='+0+
                  '&news_letter_subscribe='+1
            }).then(response => response.json())
            .then(result =>{
              console.log(result)
              if(result.flag){
                setTimeout(() =>{
                  this.props.navigation.replace("Login",{
                    value:this.props.route.params.value
                  })
                },1800);
                this.setState({isLoading:true})
              }else{
                alert(result.data);
              }
            }).catch(error=>{
              console.log(error);
            });
        }else{
          alert("Please check Your internet connection");
        }
    })
  }
}

  render() {
    return (
      <View style={styles.container}>
        <ScrollView>
          <KeyboardAvoidingView>
            <Card
              title="USER REGISTER"
              containerStyle={{
                marginTop: 40,
                width: Dimensions.get('window').width,

                marginRight: 100,
                paddingRight: 20,
                justifyContent: 'center',
              }}>
              {this.state.first_name_validation ? (
                <Input
                  placeholder="ENTER USER NAME"
                  onChangeText={value => this.setState({first_name: value})}
                  label="First name"
                />
              ) : (
                <Input
                  placeholder="User first name required"
                  label="First name"
                  onChangeText={(text) => this.setState({first_name:text})}
                  errorStyle={{color: 'red'}}
                  errorMessage="User first name"
                />
              )}
              {this.state.last_name_validation ? (
                <Input
                  placeholder="ENTER USER LAST NAME"
                  onChangeText={value => this.setState({last_name: value})}
                  label="Last name"
                />
              ) : (
                <Input
                  placeholder="Enter User Last name"
                  label="Last name"
                  onChangeText={(text) => this.setState({last_name:text})}
                  errorStyle={{color: 'red'}}
                  errorMessage="User last name is Required"
                />
              )}
              <Picker
                style={{width: 340, height: 50, marginBottom: 15}}
                selectedValue={this.state.gender}
                onValueChange={this.show.bind()}>
                <Picker.Item label="Male" value="Male" />
                <Picker.Item label="Male" value="Male" />
              </Picker>
              <DatePicker
                style={{width: 300, marginBottom: 10}}
                date={this.state.date}
                mode="date"
                placeholder="select date"
                 format="MM/DD/YYYY"
                minDate="1/1/1990"
                maxDate="1/1/2010"
                confirmBtnText="Confirm"
                cancelBtnText="Cancel"
                customStyles={{
                  dateIcon: {
                    position: 'absolute',
                    left: 0,
                    top: 4,
                    marginLeft: 0,
                  },
                  dateInput: {
                    marginLeft: 36,
                  },
                  // ... You can check the source to find the other keys.
                }}
                onDateChange={date => {
                  this.setState({date: date});
                }}
              />
              {this.state.password_validation ? (
                <Input
                  placeholder="Enter user password"
                  keyboardType="numeric"
                  // onChangeText={(value) => this.setState({password:value})}
                  rightIcon={
                    this.state.password_visiable ? (
                      <Icon
                        name="eye"
                        size={24}
                        onPress={() => this.showPassword()}
                        color="#9e9e9e"
                      />
                    ) : (
                      <Icon
                        name="eye-slash"
                        onPress={this.hidePassword}
                        size={24}
                        color="#9e9e9e"
                      />
                    )
                  }
                  onChangeText={value => this.setState({password: value})}
                  secureTextEntry={this.state.password_visiable}
                  label="Password"
                />
              ) : (
                <Input
                  placeholder="Enter Your password"
                  secureTextEntry={this.state.password_visiable}
                  rightIcon={
                    this.state.password_visiable ? (
                      <Icon
                        name="eye"
                        size={24}
                        onPress={() => this.showPassword()}
                        color="#9e9e9e"
                      />
                    ) : (
                      <Icon
                        name="eye-slash"
                        size={24}
                        onPress={() => this.hidePassword()}
                        color="#9e9e9e"
                      />
                    )
                  }
                
                  label="Password"
                  errorStyle={{color: 'red'}}
                  errorMessage="Password field is Required"
                  onChangeText={(value) => this.setState({password:value})}
                />
              )}
              {this.state.confirmPassword_validation ? (
                <Input
                  placeholder="CONFIRM PASSWORD"
                  keyboardType="number-pad"
                  rightIcon={
                    this.state.password_visiable ? (
                      <Icon
                        name="eye"
                        size={20}
                        onPress={this.showCpassword}
                        color="#9e9e9e"
                      />
                    ) : (
                      <Icon
                        name="eye-slash"
                        onPress={this.hideCpassword}
                        size={20}
                        color="#9e9e9e"
                      />
                    )
                  }
                  onChangeText={value =>
                    this.setState({confirmPassword: value})
                  }
                  secureTextEntry={this.state.password_visiable}
                  label="Confirm Password"
                />
              ) : (
                <Input
                  placeholder="Confirm password"
                  errorStyle={{color: 'red'}}
                  errorMessage="Confirm password is required"
                  keyboardType="number-pad"
                  rightIcon={
                    this.state.password_visiable ? (
                      <Icon
                        name="eye"
                        size={20}
                        onPress={this.showCpassword}
                        color="#9e9e9e"
                      />
                    ) : (
                      <Icon
                        name="eye-slash"
                        onPress={this.hideCpassword}
                        size={20}
                        color="#9e9e9e"
                      />
                    )
                  }
                  onChangeText={value =>
                    this.setState({confirm_password: value})
                  }
                  secureTextEntry={this.state.password_visiable}
                  label="Confirm Password"
                />
              )}
              {this.state.email_validation ? (
                <Input
                  placeholder="ENTER USER EMAIL"
                  onChangeText={value => this.setState({email: value})}
                  label="Email"
                />
              ) : (
                <Input
                  placeholder="ENTER USER EMAIL"
                  errorStyle={{color: 'red'}}
                  errorMessage="Please Check your Email address"
                  onChangeText={value => this.setState({email: value})}
                  label="Email"
                />
              )}
              {this.state.number_validation ? (
                <Input
                  placeholder="ENTRE USER MOBILE NUMBER"
                  onChangeText={value => this.setState({number: value})}
                  label="Number"
                />
              ) : (
                <Input
                  errorStyle={{color: 'red'}}
                  errorMessage="User Contact number is required"
                  placeholder="ENTRE USER MOBILE NUMBER"
                  onChangeText={value => this.setState({number: value})}
                  label="Number"
                />
              )}
              {/* <Input placeholder="ENTER PASSWORD" label="Password" />
              <Input placeholder="CONFIRM PASSWORD" label="Confirm Password" /> */}

              {
                this.state.isLoading ? (
                    <Button title="submit" onPress={this.register} loading />
                  ):(
                    <Button title="submit" onPress={this.register}  />
                  )
              }
            </Card>
          </KeyboardAvoidingView>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
