import 'react-native-gesture-handler';
import React from 'react';

import {SafeAreaView, ScrollView, Dimensions} from 'react-native';

import {NavigationContainer} from '@react-navigation/native';

import {createStackNavigator} from '@react-navigation/stack';

import {createDrawerNavigator} from '@react-navigation/drawer';

import Login from './src/screens/Login';

import Splash from './src/screens/splash';

import Home from './src/screens/Home';

import Products from './src/screens/products';

import ProductDetails from './src/screens/productDetails';

import Register from './src/screens/register';

import Cart from './src/screens/cart';

import Icon from 'react-native-vector-icons/FontAwesome';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

function DrawerRoute() {
  return (
    <Drawer.Navigator
      hideStatusBar
      drawerContentOptions={{
        activeTintColor: '#fff',
        tintColor: '#fff',
        itemStyle: {marginVertical: 30, marginBottom: 13, marginTop: 10},
        labelStyle: {
          color: '#fff',
          justifyContent: 'space-around',
        },
      }}
      statusBarAnimation
      drawerStyle={{
        backgroundColor: '#2962ff',
        width: 250,
      }}>
      <Drawer.Screen
        name="Home"
        component={Home}
        options={{
          drawerIcon: () => <Icon name="home" size={18} color="#fff" />,
        }}
      />
      <Drawer.Screen
        name="Cart"
        component={Cart}
        options={{
          drawerIcon: () => (
            <Icon name="shopping-cart" size={18} color="#fff" />
          ),
        }}
      />
    </Drawer.Navigator>
  );
}

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator headerMode="none" initialRouteName="Home">
        <Stack.Screen name="Splash" component={Splash} />
        <Stack.Screen name="Products" component={Products} />
        <Stack.Screen name="Register" component={Register} />
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="ProductDetails" component={ProductDetails} />
        <Stack.Screen name="Home" component={DrawerRoute} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
